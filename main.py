from tkinter import *


def on_button_clicked():
    print("I got clicked")
    new_text = input_field.get()
    my_label.config(text=new_text)


window = Tk()
window.title("My first GUI")
window.minsize(width=500, height=300)
window.config(padx=20, pady=20)

# LABEL
my_label = Label(text="I 'm a label", font=("Arial", 24, "bold"))
my_label.config(text="New text")
# my_label.pack()
# my_label.place(x=0, y=200)
my_label.grid(column=0, row=0)

# BUTTON
button = Button(text="Click me!", command=on_button_clicked)
# button.pack()     # Throws error when u have used .grid() previously
button.grid(column=1, row=1)

new_button = Button(text="New Button")
new_button.grid(column=3, row=0)

# ENTRY
input_field = Entry(width=10)
print(input_field.get())
# input_field.pack()
input_field.grid(column=4, row=3)


window.mainloop()
