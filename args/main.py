#   *args      unlimited positional ARGumentS
def add(*args):
    print(args)     # returns (1, 2, 3, 4)
    print(args[0])  # returns 1
    result = 0
    for n in args:
        result += n
    return result


print(add(1, 2, 3, 4))


# **args        many KeyWord ARGumentS
def calc(n, **kwargs):
    print(kwargs)   # returns a dict, i.e. {'add': 3, 'multiply': 5}
    # for (key, value) in kwargs.items():
    #     print(key, "\t", value)

    print(kwargs["add"])    # returns 3
    n += kwargs["add"]
    n *= kwargs["multiply"]

    print(n)


calc(2, add=3, multiply=5)


class Car:
    def __init__(self, **kwargs):
        self.make = kwargs.get("make")      # .get() will not throw error when not initialized, it returns None
        self.model = kwargs["model"]       # a dictionary lookup, will throw an error


my_car = Car(model="gt-r")

print(my_car.model)
