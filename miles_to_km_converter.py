from tkinter import *


def on_button_clicked():
    input_miles = float(miles_input.get())
    result_km = int(input_miles * 1.609)
    km_result_label.config(text=result_km)


window = Tk()
window.title("Miles to KM converter")
window.config(pady=20, padx=20)

miles_input = Entry()
miles_input.grid(column=1, row=0)

miles_label = Label(text="Miles")
miles_label.grid(column=2, row=0)

equal_to_label = Label(text="is equal to")
equal_to_label.grid(column=0, row=1)

km_result_label = Label(text="0")
km_result_label.grid(column=1, row=1)

km_label = Label(text="km")
km_label.grid(column=2, row=1)

calculate_button = Button(text="Calculate", command=on_button_clicked)
calculate_button.grid(column=1, row=3)


window.mainloop()
